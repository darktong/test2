FROM rust:1.56-alpine3.14 AS builder

WORKDIR /app
COPY . .
RUN cargo build --release

FROM alpine:3.14
WORKDIR /app
COPY --from=builder /app/target/release/test2 /app/test2
# CMD [ "/app/test2" ]
